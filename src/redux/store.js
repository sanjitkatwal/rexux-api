import {createStore} from "redux";
import reducers from './reducers/rootReducer';

const store = createStore(reducers,{},
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ); //first arg is reducer and second is state like redux thunk or saga

export default store;